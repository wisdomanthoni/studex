<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Files;
use Illuminate\Support\Facades\URL;
use App\Student;
use Auth ;


class StudentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //  return view('home');
        //  $students = \App\Student::all();
        $id = Auth::user()->id ;
          $students = User::find($id)->students;
        return view('home')->with('students', $students);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       $this->validate($request,[
          'name' => 'required|max:225|min:5',
          'department' => 'required',
          'dob' => 'required',
          'level' => 'required',
           'gender' => 'required'
        ]);
        
        $student = new Student;

        $student->name = $request->input('name');
        $student->user_id = Auth::user()->id;
        $student->department = $request->input('department');
        $student->dob = $request->input('dob');
         $student->level = $request->input('level');
          $student->gender= $request->input('gender');
           
          if(INPUT::hasFile('pic')){
              $file = Input::file('pic');
             $file->move(public_path().'/img/',$file->getClientOriginalName());
             $url = URL::to("/").'/img/'.$file->getClientOriginalName();
          }

          $student->pic = $url ;
           $student->save();

        return redirect('/home')->with('status', 'Student Added');
        
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       $student = Student::find($id);

       return view('view')->with('student',$student);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
          'name' => 'required|max:225|min:5',
          'department' => 'required',
          'dob' => 'required',
          'level' => 'required',
           'gender' => 'required'
        ]);
        
        $student = Student::find($id);

        $student->name = $request->input('name');
        $student->user_id = Auth::user()->id;
        $student->department = $request->input('department');
        $student->dob = $request->input('dob');
         $student->level = $request->input('level');
          $student->gender= $request->input('gender');
           
          if(INPUT::hasFile('pic')){
              $file = Input::file('pic');
             $file->move(public_path().'/img/',$file->getClientOriginalName());
             $url = URL::to("/").'/img/'.$file->getClientOriginalName();
          }

          $student->pic = $url ;
           $student->save();

        return redirect('/home')->with('status', 'Student Updated');
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         $student = Student::find($id);
         $student->delete();
  
           return redirect('/home')->with('status', 'Student Deleted');
        

    }
}
