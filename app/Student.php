<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Student extends Model
{
  // protected $fillable = array('name','user_id','description','age','school');

  
   //DEFINE RELATIONSHIPS
    public function user(){
         return $this->belongsTo('App\User');
    }
}
