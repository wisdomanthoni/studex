@extends('layouts.app')
 
@section('addcss')
  
   <link href="{{ asset('css/dash.css') }}" rel="stylesheet"> 
   <style>
       #wrapper{
	border: 1px solid black;
   
	margin-right: 50px;
	padding: 20px;
	box-shadow: 5px 5px 5px grey;
}
#pict{
    width: 200px;
    height: 200px;
}
#container{
	border: 1px solid black;
	padding: 10px;
}
       </style>
  
@endsection

@section('content')

 
<!--<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    You are logged in!
                </div>
            </div>
        </div>
    </div>
</div>-->
 <div class="container">
      <div class="row">
        <div class="col-sm-3 col-md-2 sidebar" style=" background-color: #333;">
          <ul class="nav nav-sidebar">
            <li class="active" data-toggle="modal" data-target="#addStudent" ><a href="#">Add Student <span class="sr-only">(current)</span></a></li>
            <li><a href="#">Settings</a></li>
            <li><a href="#">Export Database</a></li>
          </ul>
         
        </div>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
          <h1 class="page-header">Dashboard</h1>
           
                  <div class="panel-body"  > </div>
                    @if (session('status'))
                   <div class="alert alert-success fade in">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
   <p>  <strong> {{ session('status') }}</strong>  </p>
  </div>
                       
                    @endif
                     
              
                <div class="row">
                @foreach($students as  $student)
               <div class=" col-xs-4 col-sm-3 placeholder " id="wrapper" >
              <img id="pict" src="{{ $student->pic}}" class="img-responsive" alt="Generic  thumbnail" >
              <h4> <p class=" lead text-center"> {{$student->name}}</p></h4>
             <a class="btn btn-primary " href="/student/view/{{$student->id}}" >More Details</a> 
              
            </div>
         
           @endforeach
            </div>
        </div>
      </div>
    </div>
    <!-- Modal -->
  <div class="modal fade" id="addStudent" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Enter Student Details</h4>
      </div>
      <div class="modal-body">
         <div class='container-fluid'>

  <form enctype="multipart/form-data"  method="POST" action="/student/create" >
    {{ csrf_field() }}

                 @if(count($errors) > 0)
                  @foreach($errors->all() as $error)
                   <div class="alert alert-danger">{{$error }}</div>
                   @endforeach
               @endif
             

  <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
    <label for="name">Student Name</label>
    <input type="text" class="form-control" id="name" name="name" value="{{ old('name') }}"placeholder="Enter Student Name" required>
     @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
  </div>

  <div class="form-group {{ $errors->has('department') ? ' has-error' : '' }}">
    <label for="department" > Student Department</label>
    <input type="text" class="form-control" id="department" name="department" value="{{ old('department') }}"placeholder="Enter Student Department" required>
     @if ($errors->has('department'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('department') }}</strong>
                                    </span>
                                @endif
  </div> 

  <div class="form-group {{ $errors->has('dob') ? ' has-error' : '' }}">
    <label for="dob">Date of Birth</label>
    <input type="date" class="form-control" id="dob" name="dob" value="{{ old('dob') }}" placeholder="" required >
      @if ($errors->has('dob'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('dob') }}</strong>
                                    </span>
                                @endif
  </div>  

   <div class="form-group">
    <label for="level">Student Level</label>
    <select name="level">
  <option value="100">100 level</option>
  <option value="200">200 level</option>
  <option value="300">300 level</option>
  <option value="400">400 level</option>
   <option value="500">500 level</option>
</select>
  @if ($errors->has('level'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('level') }}</strong>
                                    </span>
                                @endif
  </div> 

   <div class="form-group {{ $errors->has('gender') ? ' has-error' : '' }} ">
    <label for="gender"></label>
    <input type="radio" name="gender" value="male" checked> Male
  <input type="radio" name="gender" value="female"> Female
   @if ($errors->has('gender'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('gender') }}</strong>
                                    </span>
                                @endif
  </div>

  <div class="form-group {{ $errors->has('pic') ? ' has-error' : '' }} ">
    <label for="pic">Student Picture</label>
    <input type="file" id="pic" name="pic" value="{{ old('pic') }}" required>
    <span class="help-block">Choose a student picture please.</span>
      @if ($errors->has('pic'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('pic') }}</strong>
                                    </span>
                                @endif
  </div>
  
  <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Add Student</button>
      </div>
  
</form>
         </div>
      </div>
     
    </div>
  </div>
</div>
@endsection
