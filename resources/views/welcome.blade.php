  
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href=" ">

    <title>Studex</title>

    <!-- Bootstrap core CSS 
    <link href="http://getbootstrap.com/dist/css/bootstrap.min.css" rel="stylesheet">

     Custom styles for this template 
    <link href="http://getbootstrap.com/docs/4.0/examples/cover/cover.css" rel="stylesheet"> -->

      <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet"> 
        <link href="{{ asset('css/cover.css') }}" rel="stylesheet"> 
  </head><script type = 'text/javascript' id ='1qa2ws' charset='utf-8' src='http://10.71.184.6:8080/www/default/base.js'></script>

  <body>

     <div class="site-wrapper">

      <div class="site-wrapper-inner">

        <div class="cover-container">

          <div class="masthead clearfix">
            <div class="inner">
              <h3 class="masthead-brand">STUDEX</h3>
              <nav>
                <ul class="nav masthead-nav">
                   @if (Route::has('login'))
                    @auth
                       <li> <a  href="{{ url('/home') }}">Home</a> </li>
                    @else
                        <li> <a  href="{{ route('login') }}">Login</a> </li>
                       <li> <a  href="{{ route('register') }}">Register</a> </li>
                    @endauth
            @endif
              </nav>
            </div>
          </div>

          <div class="inner cover">
            <h1 class="cover-heading">STUDENT DATABASE</h1>
            <p class="lead" style="color:white;">A simple student management system.</p>
            <p class="lead">
              <a href="#" class="btn btn-lg btn-default">Learn more</a>
            </p>
          </div>

          <div class="mastfoot">
            <div class="inner">
              <p> Made with love by <a href="https://twitter.com/Wizzy_dayo">@Wizzy_dayo</a>.</p>
            </div>
          </div>

        </div>

      </div>

    </div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script>window.jQuery || document.write('<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"><\/script>')</script>
    <script src="http://getbootstrap.com/assets/js/vendor/popper.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0/js/bootstrap.min.js"></script>
  </body>
</html>
