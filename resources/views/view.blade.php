@extends('layouts.app')
@section('addcss')
 <style>
   
#pic{
    width: 300px;
   
}
/* add your CSS here */
#wrapper{
	border: 1px solid black;
    width : 350px;
	margin: auto;
	padding: 20px;
	box-shadow: 5px 5px 5px grey;
}

#container{
	border: 1px solid black;
	padding: 10px;
}
       </style>
       @endsection
@section('content')
<div >
 <h1>Showing {{ $student->name }}</h1>

    <div class=" " id="wrapper" >
         <div> <a class="btn btn-default " href="{{ URL::to('home') }}">Back</a> 
          <a class="btn btn-primary " href="#"  data-toggle="modal" data-target="#editStudent">Edit</a> 
           <a class="btn btn-danger " href="#" data-toggle="modal" data-target="#delete">Delete</a> 
        </div>
             <p> <img id="pic" class="text-center" src="{{ $student->pic}}" class="img-responsive" alt="Generic  thumbnail" > </p>
      <div id="container">  
          <h4> <p class=" lead text-center"> Name: {{$student->name}}</p></h4>
               <h4> <p class=" text-center"> Department: {{$student->department}}</p></h4>
                <h4>  <p class="  text-center">Gender: {{$student->gender}}</p></h4>
            <h4 > <p class="  text-center">  Level: {{$student->level}}</p></h4>
             <h4>  <p class=" text-center">D.O.B: {{$student->dob}}</p></h4>
      </div>
            
    </div>

    <!-- Modal -->
<div class="modal fade" id="delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
        <form action="/student/delete/{{$student->id}}" method="GET">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Remove  {{$student->name}}  </h4>
      </div>
      <div class="modal-body">
       Are You sure you want to delete {{$student->name}} from your student database?
      </div>
      <div class="modal-footer">
         <a href="">  <button type="submit" class="btn btn-primary btn-large" >Yes</button></a>
        <button type="button" class="btn btn-default btn-large" data-dismiss="modal">No</button>
       
      </div>
    </div>
</form>
  </div>
</div>

      <!-- Modal -->
  <div class="modal fade" id="editStudent" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Enter Student Details</h4>
      </div>
      <div class="modal-body">
         <div class='container-fluid'>

  <form enctype="multipart/form-data"  method="POST" action="/student/edit/{{$student->id}}" >
    {{ csrf_field() }}

                 @if(count($errors) > 0)
                  @foreach($errors->all() as $error)
                   <div class="alert alert-danger">{{$error }}</div>
                   @endforeach
               @endif
             

  <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
    <label for="name">Student Name</label>
    <input type="text" class="form-control" id="name" name="name" value=" {{$student->name}}"placeholder="Enter Student Name" required>
     @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
  </div>

  <div class="form-group {{ $errors->has('department') ? ' has-error' : '' }}">
    <label for="department" > Student Department</label>
    <input type="text" class="form-control" id="department" name="department" value=" {{$student->department}}"placeholder="Enter Student Department" required>
     @if ($errors->has('department'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('department') }}</strong>
                                    </span>
                                @endif
  </div> 

  <div class="form-group {{ $errors->has('dob') ? ' has-error' : '' }}">
    <label for="dob">Date of Birth</label>
    <input type="date" class="form-control" id="dob" name="dob" value=" {{$student->dob}}" placeholder="" required >
      @if ($errors->has('dob'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('dob') }}</strong>
                                    </span>
                                @endif
  </div>  

   <div class="form-group">
    <label for="level">Student Level</label>
    <select name="level">
  <option value="100">100 level</option>
  <option value="200">200 level</option>
  <option value="300">300 level</option>
  <option value="400">400 level</option>
   <option value="500">500 level</option>
</select>
  @if ($errors->has('level'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('level') }}</strong>
                                    </span>
                                @endif
  </div> 

   <div class="form-group {{ $errors->has('gender') ? ' has-error' : '' }} ">
    <label for="gender"></label>
    <input type="radio" name="gender" value="male" checked> Male
  <input type="radio" name="gender" value="female"> Female
   @if ($errors->has('gender'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('gender') }}</strong>
                                    </span>
                                @endif
  </div>

  <div class="form-group {{ $errors->has('pic') ? ' has-error' : '' }} ">
    <label for="pic">Student Picture</label>
    <input type="file" id="pic" name="pic" value="{{ old('pic') }}" required>
    <span class="help-block">Choose a student picture please.</span>
      @if ($errors->has('pic'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('pic') }}</strong>
                                    </span>
                                @endif
  </div>
  
  <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Update Student</button>
      </div>
</form>
         </div>
      </div>
     
    </div>
  </div>
</div>
@endsection